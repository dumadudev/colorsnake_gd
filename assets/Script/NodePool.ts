
export class NodePool {
     //Batch initialize object pool
    static batchInitObjPool(thisO, objArray) {
        for (let index = 0; index < objArray.length; index++) {
            let objinfo = objArray[index];
            this.initObjPool(thisO, objinfo);
        }
    }
    //Initialize the object pool
    static initObjPool(thisO, objInfo) {
        let name = objInfo.name;
        let poolName = name + 'Pool';
        thisO[poolName] = new cc.NodePool();
        let initPollCount = objInfo.initPollCount;

        for (let ii = 0; ii < initPollCount; ++ii) {
            let nodeO = cc.instantiate(objInfo.prefab); // Create node
            thisO[poolName].put(nodeO); // Put into the object pool through the putInPool interface
        }
    }

    //Generate node
    static genNewNode(pool, prefab, nodeParent) {
        let newNode = null;

        if (pool.size() > 0) { // Determine whether there are free objects in the object pool through the size interface
            newNode = pool.get();
        } else { // If there are no free objects, that is, when there are not enough spare objects in the object pool, we use cc.instantiate to recreate
            newNode = cc.instantiate(prefab);
        }
        nodeParent.addChild(newNode);
        return newNode;
    }
    //Return to the object pool
    static backObjPool(thisO, nodeInfo) {
        let poolName = nodeInfo.name + 'Pool';
        // cc.log("长度 ============= "+thisO[poolName].size()+" --- "+nodeInfo.name);
        thisO[poolName].put(nodeInfo);
    }
}
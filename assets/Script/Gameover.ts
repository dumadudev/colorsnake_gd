
import {Main} from "./Main";
import { Result } from "./Result";

const {ccclass, property} = cc._decorator;

@ccclass
export class Gameover extends cc.Component {
    private main:Main = null;

    @property(cc.Node)
    btn_home:cc.Node = null;

    @property(cc.Node)
    btn_share:cc.Node = null;

    @property(cc.Node)
    btn_replay:cc.Node = null;
   
    @property(cc.Label)
    finalscore:cc.Label=null;
    
    @property(cc.AudioClip)
    mysound:cc.AudioClip=null;

    onLoad(){
        
        this.btn_home.on(cc.Node.EventType.TOUCH_END, ()=>{
            cc.audioEngine.playEffect(this.mysound,false);
            cc.director.loadScene("menu"); 
        })
        this.btn_replay.on(cc.Node.EventType.TOUCH_END, ()=>{
            cc.sys.localStorage.setItem('counter',JSON.stringify(1));
            cc.sys.localStorage.setItem('revival',JSON.stringify(1));
            cc.audioEngine.playEffect(this.mysound,false);
            cc.director.loadScene("game"); 
        })

    }
    update(){
        var bool=JSON.parse(cc.sys.localStorage.getItem('bool')); 
    if (bool=="1"){
    var hs=JSON.parse(cc.sys.localStorage.getItem('ColorSnakehighscore'));
        this.finalscore.string= hs;
        console.log(hs);
    }
    cc.sys.localStorage.setItem('bool',JSON.stringify('0'));
    var val=cc.sys.localStorage.getItem('sound');
      if(val==="0"){
          cc.audioEngine.pauseAllEffects();
      }
    }
    
    }
    
    


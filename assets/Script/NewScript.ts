import {Main} from "./Main";
import {Shake} from "./Shake";

const {ccclass, property} = cc._decorator;

@ccclass
export class Result extends cc.Component {
    private main:Main = null;
    @property(cc.Label)
    score:cc.Label = null;
    
    init(main:Main){
        this.score.node.active = false;
        cc.sys.localStorage.setItem('score',this.score.string);
    }

getScore(num:number){
    this.score.string = (num + Number(this.score.string)).toString();
    
}
}
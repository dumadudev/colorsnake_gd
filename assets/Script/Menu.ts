
import {Main} from "./Main";

const {ccclass, property} = cc._decorator;

@ccclass
export class Menu extends cc.Component {
    private main:Main = null;

    @property(cc.AudioClip)
    mysound:cc.AudioClip=null;
    
    @property(cc.Label)
    highscore:cc.Label = null;

    @property(cc.Node)
    btn_start:cc.Node = null;

    @property(cc.Node)
    btn_soundoff: cc.Node = null;

    @property(cc.Node)
    btn_soundon: cc.Node = null;
 
    @property(cc.Node)
    btn_musicoff: cc.Node = null;

    @property(cc.Node)
    btn_musicon: cc.Node = null;

    onLoad(){
        console.log("Entered");
        if (gdsdk !== 'undefined' && gdsdk.preloadAd !== 'undefined') {
            gdsdk
            .preloadAd()
            .then(response => {
                console.log("rewarded video preloaded");
                console.log(response);
                
                cc.sys.localStorage.setItem('Loaded',JSON.stringify('1'));
            // A rewarded ad can be shown to user when he/she clicked it.
            })
            .catch(error => {
                console.log(error);
                console.log(error.message);
                console.log("no_ad");
                cc.sys.localStorage.setItem('Loaded',JSON.stringify('0'));
            // Any Rewarded ad is not available to user.
            });
        }
       
    var hs=JSON.parse(cc.sys.localStorage.getItem('ColorSnakehighscore'));
    var os=JSON.parse(cc.sys.localStorage.getItem('ColorSnakeoverallhighscore'));
    if (os==null){
        this.highscore.string= "0";
    }
    else
    if(os<hs){
         this.highscore.string= hs;
            }else{
                this.highscore.string= os;
                 }

cc.sys.localStorage.setItem('ColorSnakeoverallhighscore',this.highscore.string);


        this.btn_start.on(cc.Node.EventType.TOUCH_END, ()=>{
            cc.sys.localStorage.setItem('revival',JSON.stringify(1));
            cc.sys.localStorage.setItem('counter',JSON.stringify(1));
            cc.audioEngine.playEffect(this.mysound,false);
            var help=cc.sys.localStorage.getItem('Learned');
            if(help==1){
            cc.director.loadScene("game"); 
            }
            else
            {
                cc.director.loadScene("StartGame"); 
            }
        });

        this.btn_soundon.on("touchend", this.soundOff, this);
        this.btn_soundoff.on("touchend", this.soundOn, this);
        this.btn_musicon.on("touchend", this.musicOff, this);
        this.btn_musicoff.on("touchend", this.musicOn, this);

        
    }
    
     gameStart(){
         this.node.active = false;
        
     }
       
     private soundOn() {

            this.btn_soundon.active=true;
            this.btn_soundoff.active=false;
            if(this.btn_soundoff.active==false){
                cc.sys.localStorage.setItem('sound',JSON.stringify(1));
            }else{
                cc.sys.localStorage.setItem('sound',JSON.stringify(0)); 
            }
            
      }

      private soundOff() {

        this.btn_soundon.active=false;
        this.btn_soundoff.active=true;
        if(this.btn_soundoff.active==true){
            cc.sys.localStorage.setItem('sound',JSON.stringify(0));
        }else{
            cc.sys.localStorage.setItem('sound',JSON.stringify(1)); 
        }
        
  }

  private musicOn() {

    this.btn_musicon.active=true;
    this.btn_musicoff.active=false;
    if(this.btn_musicoff.active==false){
        cc.sys.localStorage.setItem('music',JSON.stringify(1));
    }else{
        cc.sys.localStorage.setItem('music',JSON.stringify(0)); 
    }
    
}

private musicOff() {

this.btn_musicon.active=false;
this.btn_musicoff.active=true;
if(this.btn_musicoff.active==true){
    cc.sys.localStorage.setItem('music',JSON.stringify(0));
}else{
    cc.sys.localStorage.setItem('music',JSON.stringify(1)); 
}

}

update(){
    var val=cc.sys.localStorage.getItem('sound');
    var val1=cc.sys.localStorage.getItem('music');
    if(val==="0"){
        cc.audioEngine.pauseAllEffects(); 
    }
    if(val==="0"){  
        this.btn_soundoff.active=true;
        this.btn_soundon.active=false;
    }else{
        this.btn_soundoff.active=false;
        this.btn_soundon.active=true;
    }
    if(val1==="0"){  
        this.btn_musicoff.active=true;
        this.btn_musicon.active=false;
    }else{
        this.btn_musicoff.active=false;
        this.btn_musicon.active=true;
    }
    } 

}



import {Data} from "./Data";
import {Main} from "./Main";
import { Result } from "./Result";

const {ccclass, property} = cc._decorator;

@ccclass
export class Board extends cc.Component {
    @property(cc.Graphics)
    graphics: cc.Graphics = null;

    @property(cc.Node)
    head_collide:cc.Node = null;

    @property(cc.AudioClip)
    bgsound:cc.AudioClip = null;

    @property(cc.AudioClip)
    snakehitsound:cc.AudioClip = null;

    private main:Main = null;

      increasePosX:number = 0;
    private increasePosY:number = Data.snakeInitPosY;

    increaseConstX:number = Data.bgSpeed; //Growth constant
    private increaseConstY:number = Data.bgSpeed; //Growth constant

    private isClick:boolean = true;    //No click
     isDie:boolean = false;  //No death
     
    snakeColor:number = 0;     //Snake color
    private sspeed:number =Data.bgSpeed;

    headposy:number=0;

    onLoad(){
        this.main.startGame();
        cc.audioEngine.playMusic(this.bgsound, false);
        this.node.parent.on(cc.Node.EventType.TOUCH_START, (event: cc.Event.EventTouch) => {
            this.increaseConstX*=-1;
            //this.graphics.lineTo(this.posX,this.posY);
            this.isClick = true;
            this.schedule(this.foodSpawningSpeed,Data.frequency,Data.ProduceFoodIntervalMax+1 ,0);
            console.log(Data.ProduceFoodIntervalMax);
            if(Data.ProduceFoodIntervalMax<=Data.maxFinalValue){
                this.unschedule(this.foodSpawningSpeed);
            }
                this.schedule(this.snakespeed,Data.frequency,Data.bgSpeed+1 ,0);
                console.log(Data.bgSpeed);
                if(Data.bgSpeed>=700){
                    this.unschedule(this.snakespeed);
            }
           
        });
        this.increasePosX = cc.winSize.width / 2;
    }
    init(main:Main){
        this.main = main;
    }

    //Calculated angle，（Angle from horizontal）
    getAngle(pos1:cc.Vec2, pos2:cc.Vec2) {
        //var pos = this.node.getPosition();
        let _angle = Math.atan2(pos1.y - pos2.y, pos1.x - pos2.x) * (180 / Math.PI);
        return _angle;
    }
    private foodSpawningSpeed(){
        Data.ProduceFoodIntervalMax-=Data.maxDecValue; 
       
    }
    private snakespeed(){
        Data.bgSpeed+=Data.speedincrement;
    }

    changeColor(){
        switch (this.snakeColor) {
            case 0:
                this.graphics.strokeColor = cc.Color.RED.fromHEX("#FF0001");
                this.head_collide.color = cc.Color.RED.fromHEX("#FF0001");
                break;
            case 1:
                this.graphics.strokeColor = cc.Color.GREEN.fromHEX("#09FF02");
                this.head_collide.color = cc.Color.GREEN.fromHEX("#09FF02");
                break;
            case 2:
                this.graphics.strokeColor = cc.Color.BLUE.fromHEX("#0054FF");
                this.head_collide.color = cc.Color.BLUE.fromHEX("#0054FF");
                break;
            case 3:
                this.graphics.strokeColor = cc.Color.YELLOW.fromHEX("#FFEF04");
                this.head_collide.color = cc.Color.YELLOW.fromHEX("#FFEF04");
                break;
        }
    }

    gameOver(){
        this.graphics.clear();
        cc.sys.localStorage.setItem('counter',JSON.stringify(0));
        this.isDie = true;
        cc.audioEngine.stopMusic();
        cc.audioEngine.playEffect(this.snakehitsound,false);
        Data.bgSpeed=300;
        Data.ProduceFoodIntervalMax=2;   
    }
   

    update(dt){
        if (!this.isDie) {
           // The collision head moves up
            this.head_collide.y += dt  * this.sspeed;
            this.headposy=this.head_collide.y;
            this.node.y -= dt * this.sspeed;
        
            if (this.increaseConstX < 0) {
                this.head_collide.x -= dt * this.sspeed;
            }else{
                this.head_collide.x += dt * this.sspeed;
            }
            if (this.isClick) {
                this.isClick = false;
            }
            // cc.log("this.increasePosX ---------",this.increasePosX);
            // cc.log("this.increasePosY ---------",this.increasePosY);


            this.graphics.moveTo(this.increasePosX,this.increasePosY);

            this.increasePosX += dt * this.increaseConstX  ;
            this.increasePosY += dt * this.increaseConstY  ;
            Data.snakeFramePos = cc.v2(this.increasePosX, Data.snakeInitPosY);
            
            this.graphics.lineTo(this.increasePosX,this.increasePosY);
            this.graphics.stroke();
            this.increaseConstY=this.sspeed;
        }
        var val=cc.sys.localStorage.getItem('music');
        if(val==="0"){
            cc.audioEngine.pauseMusic();
        }
        }    

    }


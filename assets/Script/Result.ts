import {Main} from "./Main";
import {Shake} from "./Shake";

const {ccclass, property} = cc._decorator;

@ccclass
export class Result extends cc.Component {
    private main:Main = null;

    @property(cc.AudioClip)
    mysound:cc.AudioClip=null;

    @property(cc.AudioClip)
    bgsound:cc.AudioClip=null;

    @property(cc.Label)
    score:cc.Label = null;
    
    @property(cc.Label)
    end_score:cc.Label = null;

    @property(cc.Node)
    game_over:cc.Node = null;

    @property(cc.Node)
    btn_exit:cc.Node = null;

    @property(cc.Node)
    btn_restart:cc.Node = null;
    
    @property(cc.Label)
    lb_revive_count: cc.Label=null;
    
    @property(cc.Node)
    head:cc.Node = null;

    @property(cc.Sprite)
    circle_timer:cc.Sprite= null;

    private  _autoReviveCount = 5;
    onLoad(){
        var f=cc.view.getFrameSize();
        console.log(f);
        this.node.getComponent(cc.Canvas).designResolution.width=f.width;
        this.node.getComponent(cc.Canvas).designResolution.height=f.height;
        console.log(cc.view.getVisibleSize());
        console.log(cc.view.getDesignResolutionSize());
    }
     init(main:Main){
        this.main = main;
        this.game_over.active = false;
        this.score.node.active = false;
       
        cc.sys.localStorage.setItem('score',this.score.string);
        cc.sys.localStorage.setItem('bool',JSON.stringify('0'));
        var rev=cc.sys.localStorage.getItem('revival');
        this.btn_exit.on(cc.Node.EventType.TOUCH_END, ()=>{
            cc.audioEngine.playEffect(this.mysound,false);
             cc.director.loadScene("gameover");
        })
        if(rev==="1"){
        this.btn_restart.on(cc.Node.EventType.TOUCH_END, ()=>{
            cc.game.pause();
            cc.sys.localStorage.setItem('counter',JSON.stringify(1));
            cc.sys.localStorage.setItem('revival',JSON.stringify(0));
            if (gdsdk !== 'undefined' && gdsdk.showAd !== 'undefined') {
                gdsdk.showAd('rewarded')
                .then(response => {
                    // Reward the player here.
                    console.log(response)
                    console.log("thenresponse")
                    console.log('revive done');
                    cc.audioEngine.playEffect(this.mysound,false);
                    cc.game.resume;
                    this.revive();
                    
                    
                })
                .catch(error => {
                  // An error catched. Please don't give reward here.
                  cc.game.resume;
                  this.unschedule(this.autoReviveCountFn);
                //   this.gameOver();
                cc.director.loadScene("gameover");
                  console.log(error)
                  console.log("errorresponse")
                });
              }
            
    })
}
}
     private scoreupdate(){

         cc.director.loadScene('game');  

     }

    getScore(num:number){
        this.score.string = (num + Number(this.score.string)).toString();
        
    }

    gameStart(){
        this.score.node.active = true;
    }

    gameOver(){
        var rev=cc.sys.localStorage.getItem('revival');
         var val=cc.sys.localStorage.getItem('counter');
        if(val==="0"){
            this._autoReviveCount = 5;
            this.autoReviveCountFn();
            this.schedule( this.autoReviveCountFn, 1, this._autoReviveCount + 1, 0);
            }else{
              this.unschedule(this.autoReviveCountFn);
             }
        if(rev==="1"){
            
        this.game_over.active = true;
        // showDynamicInterstitial();
        
        }else{

            cc.director.loadScene("gameover");
           
        }
        this.end_score.string = this.score.string;
        cc.sys.localStorage.setItem('ColorSnakehighscore',this.end_score.string);
        cc.sys.localStorage.setItem('bool',JSON.stringify('1'));
        this.score.node.active = false;

        this.scheduleOnce(()=>{
            let shake:Shake = Shake.create(0.6,10,10);
            this.game_over.runAction(shake);
            if (cc.sys.os == cc.sys.OS_ANDROID) {
                navigator.vibrate(600);
            }
        },0)

    }
    update(){
        this.circle_timer.fillRange=-((this._autoReviveCount/5));
        var val=cc.sys.localStorage.getItem('sound');
        if(val==="0"){
            cc.audioEngine.pauseAllEffects();
        }
    }
    revive(){
        // cc.director.resume();
        cc.game.resume();
        this.unschedule(this.autoReviveCountFn);
        this.game_over.active=false;
        this.gameStart();
        this.main.foodGroup.gameStart();
        cc.director.getPhysicsManager().enabled = true;
        this.head.active=true;
        this.main.colliderHead.gameStart();
        this.main.board.isDie=false;
        this.head.setPosition(cc.winSize.width / 2,this.main.board.headposy);
        this.main.board.increasePosX = cc.winSize.width / 2;
        cc.audioEngine.playMusic(this.bgsound, false);
        //console.log("position:",this.main.board.headposy);
    }  
         
    
           
//After revive Countdown ends
    private autoReviveCountFn() {
        this._autoReviveCount--;
        this.lb_revive_count.string = `${this._autoReviveCount}`;
        if (this._autoReviveCount < 1) {
            cc.director.loadScene("leaderboardwithscore");
            this._autoReviveCount=0
            cc.director.loadScene("gameover");

        }
       
        //cc.director.loadScene("gameover");
        // showDynamicInterstitial();
    }
}


// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {
            @property(cc.ParticleSystem)
            particle: cc.ParticleSystem=null;
     
        toggleParticlePlay () {
            var myParticle = this.particle;
            if (myParticle.particleCount > 0) { // check if particle has fully plaed
                myParticle.stopSystem(); // stop particle system
            } else {
                myParticle.resetSystem(); // restart particle system
            }
        }
}
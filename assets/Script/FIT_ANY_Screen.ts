

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {


    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        var f=cc.view.getFrameSize();
        console.log(f);
        this.node.getComponent(cc.Canvas).designResolution.width=f.width;
        this.node.getComponent(cc.Canvas).designResolution.height=f.height;
        console.log(cc.view.getVisibleSize());
        console.log(cc.view.getDesignResolutionSize());
    }

    // start () {

    // }

    // update (dt) {}
}

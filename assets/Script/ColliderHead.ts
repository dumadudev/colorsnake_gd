import {Food} from "./Food";
import {Main} from "./Main";
import {Data} from "./Data";
import { Board } from "./Board";
const {ccclass, property} = cc._decorator;

@ccclass
export class ColliderHead extends cc.Component {
    private main:Main = null;

    isGameStart:boolean = false;    //The game did not start
    onLoad(){
        this.node.x = cc.winSize.width / 2;
    }

    init(main:Main){
        this.main = main;
    }
    
    onCollisionEnter(other, self) {
        if (other.tag == 1) {
            // cc.log("Eaten ============================ ");
            if (other.node.getComponent(Food).foodColor == this.main.board.snakeColor) {
                other.node.active =false;    //Food disappeared
                this.main.result.getScore(10);
                 const action =cc.sequence(cc.scaleTo(0.2, 2, 2), cc.scaleTo(0.2, 1, 1));
                 this.node.runAction(action.clone());
                 console.log("cOllision takes place");
                let temp = this.changeColor(this.main.board.snakeColor);
                if (temp != this.main.board.snakeColor) {
                    this.main.board.snakeColor = temp;
                    this.main.board.changeColor();
                }
            }else{
                cc.sys.localStorage.setItem('counter',JSON.stringify(0));
                // cc.log("Gameover ！！！")
                this.main.gameOver();
            }
        }else if (other.tag == 9) {
            cc.sys.localStorage.setItem('counter',JSON.stringify(0));
            //Hit the wall
            this.main.gameOver();
         }
         else if (other.tag == 10) {
           // The snake that appears when the menu is displayed
            // if (!this.isGameStart) {
            //  this.main.board.increaseConstX *=-1;
            // }
         }
    }

    changeColor(oldColor:number){
        let randomNum = Math.floor(Math.random() * 10);
        cc.log("Whether to change the color", randomNum);
        //Change color
        if (randomNum >= Data.changeColorProbability) {
            //Random color
            let newColor:number = oldColor;
            while (newColor == oldColor) {
                newColor = Math.floor(Math.random() * 4);
            }
            return newColor;
        }
        return oldColor;
    }

    gameStart(){
        this.isGameStart = true;
    }

    gameOver(){
        this.node.active = false;  
        
}
}

const {ccclass, property} = cc._decorator;
@ccclass
export class Data extends cc.Component {
    
    //Background moving speed
    static bgSpeed:number = 300;
    //🐍Initial X position
    static snakeInitPosX:number = 380;
    //🐍Initial Y position
    static snakeInitPosY:number = 300;
    //Time interval between appearance of food (maximum)
    static ProduceFoodIntervalMax:number =2;
    //Time interval between appearance of food (minimum)
    static ProduceFoodIntervalMin:number = 0.2;
    //🐍Position of each frame
    static snakeFramePos:cc.Vec2 = cc.v2(0,0);
    //The probability of changing the color of the snake (greater than this value is the color change)
    static changeColorProbability:number = 5;

    static speedincrement:number = 40;
    //Time interval between incre/decre
    static frequency:number = 10;
    //maxProduce food decrement value  
    static maxDecValue:number=0.2;
    //maxProduce food final value
    static maxFinalValue:number=0.6;

   
}



//Food color
export enum Color {
    Red,
    Green,
    Blue,
    Yellow,
}




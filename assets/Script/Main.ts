import {ColliderHead} from "./ColliderHead";
import {Board} from "./Board";
import {Result} from "./Result";
import {FoodGroup} from "./FoodGroup";
import {Menu} from "./Menu";
import {Data} from "./Data";

const {ccclass, property} = cc._decorator;

@ccclass
export class Main extends cc.Component {
    @property(ColliderHead)
    colliderHead:ColliderHead = null;

    @property(Board)
    board:Board = null;

    @property(Result)
    result:Result = null;

    @property(FoodGroup)
    foodGroup:FoodGroup = null;

     @property(Menu)
     menu:Menu = null;


    onLoad(){
        var f=cc.view.getFrameSize();
        console.log(f);
        this.node.getComponent(cc.Canvas).designResolution.width=f.width;
        this.node.getComponent(cc.Canvas).designResolution.height=f.height;
        console.log(cc.view.getVisibleSize());
        console.log(cc.view.getDesignResolutionSize());
        this.colliderHead.init(this);
        this.board.init(this);
        this.result.init(this);
        //this.menu.init(this);
    }
    startGame(){
        this.colliderHead.gameStart();
        this.foodGroup.gameStart();
        //this.menu.gameStart();
        this.result.gameStart()
    }

    gameOver(){
        this.result.gameOver();
        this.board.gameOver();
        this.foodGroup.gameOver();
        this.colliderHead.gameOver();

    } 
    update () {
        var val=cc.sys.localStorage.getItem('sound');
        var val1=cc.sys.localStorage.getItem('music');
        if(val==="0"){
            cc.audioEngine.pauseAllEffects(); 
        }
    }
}
